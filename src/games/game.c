#include <strings.h>

#include "system.h"
#include "game.h"
#include "uart.h"
#include "subsys.h"
#include "timing.h"
#ifdef USE_MODULE_GAME_CONTROLLER
#include "game_controller.h"
#include "game_controller_host.h"

// button press callback functions for controller input
static void StartOrA_Pressed(controller_buttons_t buttons, void * ptr);
static void B_Pressed(controller_buttons_t buttons, void * ptr);
static void D_PadPressed(controller_buttons_t buttons, void * ptr);

static int16_t game_selected = -1;
#endif

#ifndef PLAYER1_UART
#ifndef SUBSYS_UART
#error "You must define what UART number the terminal is using in system.h: #define SUBSYS_UART num"
#endif
#define PLAYER1_UART SUBSYS_UART
#endif
// if you try to use a 2 player game without PLAYER2_UART defined in system.h
// then just double print to player1's terminal
#ifndef PLAYER2_UART
#define PLAYER2_UART PLAYER1_UART
#endif

#define MAX_GAMES 30

/// game structure
typedef struct {
    void (*callback)(int argc, char *argv[]); ///< callback (optional)
    void (*play)(void); ///< funtion to initiate playing the game
    void (*help)(void); ///< function to display help info
    void (*score)(void); ///< function to display scores (optional)
    char * name; ///< name of the game
    char * description; ///< description of the game
    uint8_t num_players; ///< number of players (1 by default)
} game_t;

game_t games[MAX_GAMES];

uint8_t game_last_index = 0xFF;
uint8_t game_id;
uint8_t playing_id = 0;

// callback for subsystem module
static void Game_Callback(int argc, char *argv[]);
// play a game
// this will remove the controller callbacks if a controller is being used
static void Play(void (*play)(void));

void Game_Init(void) {
    static uint8_t init_flag = 0;
    if (init_flag) return;
    init_flag = 1;
    game_id = Subsystem_Init("game", (version_t) 0x01010001U, Game_Callback);
#ifdef USE_MODULE_GAME_CONTROLLER
    // if the controller module exists then add button callbacks for selecting
    // games (this is done in Game_GameOver() so just call that)
    Game_GameOver();
    LogMsg(game_id, "Game Management Module Initialize, press Start or A to select a game");
#endif
}

uint8_t Game_Register(char * name, char * description,
        void (*play)(void),
        void (*help)(void)) {
    game_t * g;
    Game_Init();
    game_last_index++;
    if (game_last_index >= MAX_GAMES) return 0xFF;
    g = &games[game_last_index];
    g->help = help;
    g->play = play;
    g->name = name;
    g->description = description;
    // default num_players is 1, score callback is null, callback is null
    g->num_players = 1;
    g->score = 0;
    g->callback = 0;
    return game_last_index;
}

void Game_RegisterCallback(uint8_t game_id,
        void (*callback)(int argc, char *argv[])) {
    if(game_id >= MAX_GAMES) return; // prevent out of bounds access
    games[game_id].callback = callback;
}

void Game_RegisterHighscoreCallback(uint8_t game_id,
        void (*callback)(void)) {
    if(game_id >= MAX_GAMES) return; // prevent out of bounds access
    games[game_id].score = callback;
}

void Game_Log(uint8_t id, char * str, ...) {
    Log_Header(id, LOG_MESSAGE);
    LogStr(" %s ", games[id].name);
    va_list vars;
    va_start(vars, str);
    UART_vprintf(SUBSYS_UART, str, vars);
    va_end(vars);
}

void Game_EnableMultiPlayer(uint8_t id, uint8_t num) {
    if(id >= MAX_GAMES) return; // prevent out of bounds access
    games[id].num_players = num;
}

void Game_RegisterPlayer1Receiver(charReceiver_t rx) {
    UART_RegisterReceiver(PLAYER1_UART, rx);
}

void Game_RegisterPlayer2Receiver(charReceiver_t rx) {
    UART_RegisterReceiver(PLAYER2_UART, rx);
}

void Game_UnregisterPlayer1Receiver(charReceiver_t rx) {
    UART_UnregisterReceiver(PLAYER1_UART, rx);
}

void Game_UnregisterPlayer2Receiver(charReceiver_t rx) {
    UART_UnregisterReceiver(PLAYER2_UART, rx);
}

void Game_Callback(int argc, char *argv[]) {
    volatile uint8_t i;
    if (argc) {
        // game management commands are "help" "list" "player1" "player2"
        if (strcasecmp(argv[0], "help") == 0) {
            Game_Printf("Type '$game list' for a list of games\r\n"
                    "type '$game name play' to play\r\n"
                    "type '$game name help' for help with that game\r\n");
            return;
        }
        if (strcasecmp(argv[0], "list") == 0) {
            if(game_last_index == 0xFF) Game_Printf("No games registered\r\n");
            else {
                Game_Printf("id: name - description\r\n");
                for (i = 0; i <= game_last_index; i++) {
                    Game_Printf("%d: %s - %s\r\n", i, games[i].name, games[i].description);
                }
            }
            return;
        }
        if (strcasecmp(argv[0], "player1") == 0) {
            Game_Printf("command yet to be implemented\r\n");
            return;
        }
        if (strcasecmp(argv[0], "player2") == 0) {
            Game_Printf("command yet to be implemented\r\n");
            return;
        }
        if(game_last_index == 0xFF) return;
        for (i = 0; i <= game_last_index; i++) {
            if (strcasecmp(argv[0], games[i].name) == 0) {
                if (argc >= 2) {
                    if (strcasecmp(argv[1], "play") == 0) {
                        playing_id = i;
                        Play(games[i].play);
                        return;
                    }
                    if (strcasecmp(argv[1], "help") == 0) {
                        games[i].help();
                        return;
                    }
                    if (strcasecmp(argv[1], "score") == 0) {
                        if (games[i].score) games[i].score();
                        return;
                    }
                    if (argc >= 2) {
                        if (games[i].callback) {
                            games[i].callback(argc - 1, &argv[2]);
                        }
                    }
                }
            }
        }
    }

}

void Game_CharXY(char c, char x, char y) {
    Terminal_CharXY(PLAYER1_UART, c, x, y);
    if (games[playing_id].num_players >= 2) Terminal_CharXY(PLAYER2_UART, c, x, y);
}

void Game_Player1CharXY(char c, char x, char y) {
    Terminal_CharXY(PLAYER1_UART, c, x, y);
}

void Game_Player2CharXY(char c, char x, char y) {
    Terminal_CharXY(PLAYER2_UART, c, x, y);
}

void Game_Printf(char * str, ...) {
    // variable argument list type
    va_list vars;
    // initialize the variable argument list pointer by specifying the
    // input argument immediately preceding the variable list
    va_start(vars, str);
    UART_vprintf(PLAYER1_UART, str, vars);
    va_end(vars);
    if(games[playing_id].num_players >= 2) {
        va_start(vars, str);
        UART_vprintf(PLAYER2_UART, str, vars);
        va_end(vars);
    }
}

void Game_Player1Printf(char * str, ...) {
    va_list vars;
    va_start(vars, str);
    UART_vprintf(PLAYER1_UART, str, vars);
    va_end(vars);
}

void Game_Player2Printf(char * str, ...) {
    va_list vars;
    va_start(vars, str);
    UART_vprintf(PLAYER2_UART, str, vars);
    va_end(vars);
}

static void Cursor(uint8_t show) {
    static uint8_t echo = 0;
    if(show == 0) {
        Terminal_HideCursor(PLAYER1_UART);
        if(games[playing_id].num_players >= 2) Terminal_HideCursor(PLAYER2_UART);
        echo = Log_GetEcho();
        Log_EchoOff();
    }else {
        Terminal_ShowCursor(PLAYER1_UART);
        if(games[playing_id].num_players >= 2) Terminal_ShowCursor(PLAYER2_UART);
        if(echo) Log_EchoOn();
    }
}

void Game_HideCursor(void) {
    Cursor(0);
}

void Game_ShowCursor(void) {
    Cursor(1);
}

void Game_SetColor(enum term_color color) {
    Terminal_SetColor(PLAYER1_UART, color);
    if(games[playing_id].num_players >= 2) Terminal_SetColor(PLAYER2_UART, color);
}

void Game_DrawTile(char *tile[], char x, char y) {
    // support for "transparent" characters not implemented yet
    volatile uint8_t i = 0;
    while(tile[i]) {
        Game_CharXY(tile[i][0], x, y++);
        Game_Printf(tile[i]+1);
        i++;
    }
}

void Game_ClearScreen(void) {
    Game_Printf("\f");
}

void Game_DrawRect(char x_min, char y_min, char x_max, char y_max) {
    char x, y;
    Game_CharXY(201, x_min, y_min); // ╔
    for (x = x_min + 1; x < x_max; x++) Game_CharXY(205, x, y_min); // ═
    Game_CharXY(187, x_max, y_min); // ╗
    for (y = y_min + 1; y < y_max; y++) Game_CharXY(186, x_max, y); // ║
    // wait to transmit all the chars before continuing
    // (this could be removed if the TX buffer size is increased)
    // another way around this would be to schedule the rest of the printout
    // to happen several ms from now
    while (Game_IsTransmitting()) DelayMs(2);
    Game_CharXY(188, x_max, y_max);
    for (x = x_min + 1; x < x_max; x++) Game_CharXY(205, x, y_max); // ═
    Game_CharXY(200, x_min, y_max); // ╚
    for (y = y_min + 1; y < y_max; y++) Game_CharXY(186, x_min, y); // ║
}

void Game_FillRect(char c, char x_min, char y_min, char x_max, char y_max) {
    volatile uint8_t x, y;
    for(y = y_min; y < y_max; y++) {
        Game_CharXY(c, x_min, y);
        for(x = x_min + 1; x < x_max; x++) {
            UART_WriteByte(PLAYER1_UART, c);
            if(games[playing_id].num_players >= 2) UART_WriteByte(PLAYER2_UART, c);
        }
    }
}

void Game_ScrollDown(void) {
    Game_Printf("%cD", ASCII_ESC);
}

void Game_ScrollUp(void) {
    Game_Printf("%cM", ASCII_ESC);
}

void Game_Bell(void) {
    Game_Printf("\a");
}

uint8_t Game_IsTransmitting(void) {
    uint8_t ret;
    ret = UART_IsTransmitting(PLAYER1_UART);
    if(games[playing_id].num_players >= 2) ret += UART_IsTransmitting(PLAYER2_UART);
    return ret;
}

void Game_GameOver(void) {
#ifdef USE_MODULE_GAME_CONTROLLER
    // if the controller module exists then add button callbacks for selecting
    // games
    controller_buttons_t mask;
    mask.all_buttons = 0;
    mask.button.A = 1;
    mask.button.start = 1;
    GameControllerHost_RegisterPressCallback(7, StartOrA_Pressed, mask, 0);
    mask.all_buttons = 0;
    mask.button.B = 1;
    GameControllerHost_RegisterPressCallback(7, B_Pressed, mask, 0);
    mask.all_buttons = 0;
    mask.button.up = 1;
    mask.button.down = 1;
    mask.button.left = 1;
    mask.button.right = 1;
    GameControllerHost_RegisterPressCallback(7, D_PadPressed, mask, 0);
    // set the game_selected to -1 so we know nothing is selected yet
    game_selected = -1;
#endif
}


static void Play(void (*play)(void)) {
#ifdef USE_MODULE_GAME_CONTROLLER
    // remove controller callbacks so they don't get called while the user is
    // playing the game
    GameControllerHost_RemoveCallback(StartOrA_Pressed, 0);
    GameControllerHost_RemoveCallback(D_PadPressed, 0);
    GameControllerHost_RemoveCallback(B_Pressed, 0);
#endif
    // call the play routine that was passed to us
    play();
}

#ifdef USE_MODULE_GAME_CONTROLLER

static void DisplayGameMenu(void) {
    uint8_t i;
    Game_ClearScreen();
    Game_Printf("Welcome to the embedded game arcade!\r\n");
    if(game_last_index == 0xFF) {
        Game_Printf("Sorry, there are no games to select from");
    }else {
        Game_Printf("Use the D-Pad to select a game, Start/A to play, B for help\r\n");
        for(i = 0; i <= game_last_index; i++) {
            Game_Printf(" %d: %s - %s\r\n", i, games[i].name, games[i].description);
        }
        Game_Printf(" Exit game selection");
    }
}

static void StartOrA_Pressed(controller_buttons_t buttons, void * ptr) {
    if(game_selected == -1) {
        game_selected = 0;
        DisplayGameMenu();
        // select game 0
        Game_CharXY('>', 0, 2);
    }else {
        if(game_selected <= game_last_index) {
            Play(games[game_selected].play);
        }else if(game_selected = game_last_index+1) {
            // the user selected exit
            game_selected = -1;
            Game_ClearScreen();
        }
    }
}

static void B_Pressed(controller_buttons_t buttons, void * ptr) {
    static uint8_t in_help = 0;
    if(game_selected >= 0 && game_selected <= game_last_index) {
        if(in_help == 0 && games[game_selected].help) {
            // clear the screen and show the help
            Game_ClearScreen();
            Game_Printf("< Help for '%s', press B to go back >\r\n\n", games[game_selected].name);
            games[game_selected].help();
            in_help = 1;
        }else if(in_help == 1) {
            DisplayGameMenu();
            // select game 0
            Game_CharXY('>', 0, 2+game_selected);
            in_help = 0;
        }
    }
}

static void D_PadPressed(controller_buttons_t buttons, void * ptr) {
    if(game_selected>=0) {
        if(buttons.button.up || buttons.button.left) {
            if(game_selected > 0) {
                Game_CharXY(' ', 0, 2+game_selected);
                game_selected--;
                Game_CharXY('>', 0, 2+game_selected);
            }
        }
        if(buttons.button.down || buttons.button.right) {
            if(game_selected <= game_last_index) {
                Game_CharXY(' ', 0, 2+game_selected);
                game_selected++;
                Game_CharXY('>', 0, 2+game_selected);
            }
        }
    }
}
#endif
