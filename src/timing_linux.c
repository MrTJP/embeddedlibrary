#include <time.h>

#include "timing.h"

void Timing_Init(void) {
  return;
}

uint32_t TimeNow(void) {
  struct timespec now;
  tint_t retVal;
  if(clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &now) == 0) { // success
    retVal = now.tv_sec*1000;
    retVal += now.tv_nsec/1000000;
    return retVal;
  } else {
    return 0;
  }
}

uint32_t TimeSince(tint_t t) {
  return (TimeNow() - t);
}

void DelayMs(tint_t delay) {
  uint32_t temp;

  temp = TimeNow();
  while (TimeSince(temp) <= delay);
}

void Timing_Roll(void) {
  return;
}

uint32_t TimeNowUs(void) {
  struct timespec now;
  tint_t retVal;
  if(clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &now) == 0) { // success
    retVal = now.tv_sec*1000000;
    retVal += now.tv_nsec/1000;
    return retVal;
  } else {
    return 0;
  }
}

void DelayUs(uint32_t delay) {
  // get the time
  uint32_t timestamp;
  timestamp = TimeNowUs();
  while(TimeSinceUs(timestamp) < delay);
}

uint32_t TimeSinceUs(uint32_t t) {
  return (TimeNowUs() - t);
}
